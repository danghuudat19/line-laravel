<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use App\Services\MessengesService;


class LineController extends Controller
{
    protected $messengesService;

    public function __construct(
        MessengesService $messengesService
    )
    {
        $this->messengesService = $messengesService;
    }

    public function callBack(Request $request)
    {
        $response = $this->messengesService->relyMsg($request->events[0]['replyToken']);
        return response($response);
    }
}
