<?php


namespace App\Services;


use Illuminate\Support\Facades\Http;

class MessengesService
{
    public function relyMsg($replyToken){
        return Http::withHeaders([
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . getenv('CHANNEL_ACCESS_TOKEN'),
        ])->post('https://api.line.me/v2/bot/message/reply', [
            'replyToken' => $replyToken,
            'messages' => [
                [
                    'type' => 'text',
                    'text' => 'Hello, user',
                ]
            ]
        ]);
    }
}
